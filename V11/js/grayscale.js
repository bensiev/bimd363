var names = ['Ryan Wang','Lance Stone','Emily Lin','Tim Frank ','Jeff Wick','West Bahar','Param','Devyn','Jacob','Hannah'];
var roles = ['Chief Executive Officer & Founder','Chairman & Co-Founder','Director of Finance','Director of Productions','Director of Human Resources','Director of Operations'];
var bios = ["Ryan Wang is the founder and CEO of The Establishment, from humble beginnings he has built up The Establishment to what it is today. Wang hopes to keep The Establishment as the world's foremost energy producer, and one day supply the entire world.", "Lance Stone is the co-founder and Chairman of The Establishment. He assisted Ryan in realizing his dream of creating a power company and has worked besides him ever since. Stone tends to go in the field and visit construction sites to ensure everything is up to par", "Emily Lin is the Director of Finance for The Establishment. She handles anything and everything financially related for The Establishment, this includes planning out the The Establishment's finances decades in advance, to assessing monetary risk with new ventures. Lin's iron grip has ensured The Establishment stays above and far beyond the red."];

(function($) {
  "use strict"; // Start of use strict

  // Smooth scrolling using jQuery easing
  $('a.js-scroll-trigger[href*="#"]:not([href="#"])').click(function() {
    if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: (target.offset().top - 48)
        }, 1000, "easeInOutExpo");
        return false;
      }
    }
  });

  // Closes responsive menu when a scroll trigger link is clicked
  $('.js-scroll-trigger').click(function() {
    $('.navbar-collapse').collapse('hide');
  });

  // Activate scrollspy to add active class to navbar items on scroll
  $('body').scrollspy({
    target: '#mainNav',
    offset: 54
  });

  // Collapse Navbar
  var navbarCollapse = function() {
    if ($("#mainNav").offset().top > 100) {
      $("#mainNav").addClass("navbar-shrink");
    } else {
      $("#mainNav").removeClass("navbar-shrink");
    }
  };
  // Collapse now if page is not at top
  navbarCollapse();
  // Collapse the navbar when page is scrolled
  $(window).scroll(navbarCollapse);

})(jQuery); // End of use strict

function loadBios(){
  var displayElement = document.getElementById('displayTeam');
  for(var i = 0; i < 10; i++){
    document.getElementById('displayTeam').insertAdjacentHTML('beforeend','<div class="col-md-3">' +
        '<div class="team-block">' +
        '<div class="team-img">' +
        '<img src=' + "img/bio-pic" + i + '.jpg alt=""\>' +
        '<div class="team-content">' +
        '<h5 class="text-white">'+names[i]+'</h5>' +
        '<p class="team-meta">'+roles[0] +'</p>' +
        '</div>' +
        '<div class="overlay">' +
        '<div class="text">' +
        '<h5 class="text-white">' +names[i] +'</h5>' +
        '<p class="mb30 team-meta">'+roles[0]+'</p>' +
        '<p>'+bios[i]+'</p>' +
        '</div> </div></div></div></div></div>');
  }
}

$(document).ready(function(){
  var displayModals = document.getElementById('display-modals');
  for(var i = 0; i < 6; i++){
    console.log("Reached");
    var imageID = "image-modal" + i;
    var imageSrc = "img/head-img" + i + ".png";
    document.getElementById('display-modals').insertAdjacentHTML('beforeend',
    '<div class="modal fade" id =' + imageID + ' tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">' +
    '<div class="modal-dialog modal-dialog-centered" role="document">' +
    '<div class="modal-content">' +
    '<div class="modal-header">' +
    '<button type="button" class="close" data-dismiss="modal" aria-label="Close">' +
    '<span aria-hidden="true">×</span>' +
    '</button>' +
    '</div>' +
    '<div class="modal-body text-center" style="color:black">' +
    '<img src='+ imageSrc+' class="rounded img-fluid img-thumbnail" width="50%">' +
    '<h4>'+names[i]+'</h4>' +
    '<h6>'+ roles[i]+'</h6>' +
    '<p>'+bios[i]+ '</p>'+
    '</div>' +
    '<div class="modal-footer">' +
    '<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>' +
    '</div>' +
    '</div>' +
    '</div>' +
    '</div>')
  }
});



// <div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
//     <div class="modal-dialog modal-dialog-centered" role="document">
//     <div class="modal-content">
//     <div class="modal-header">
//     <button type="button" class="close" data-dismiss="modal" aria-label="Close">
//     <span aria-hidden="true">×</span>
// </button>
// </div>
// <div class="modal-body text-center" style="color: black">
//     <img src="img/head-img0.jpg" class="rounded img-fluid img-thumbnail" width="50%">
//     <h4>Ryan Wang</h4>
// <h6>CEO & Foudner</h6>
// <p>We must lead the development of cutting-edge technology to raise the bar for our customers. Becoming comfortable is a luxury we can’t afford ourselves, for the cost is the quality of service provided to our clients. All of our research effort and time spent creating new ideas is to continuously give more to the communities around us. We challenge ourselves to break the norm and find unique solutions and other improvements to offer unrivaled value to our customers.</p>
// </div>
// <div class="modal-footer">
//     <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
//     </div>
//     </div>
//     </div>
//     </div>